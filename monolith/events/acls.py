
import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

Pkey = PEXELS_API_KEY
Okey = OPEN_WEATHER_API_KEY
def get_photo(city, state):
    url =f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {'Authorization': Pkey}
    response = requests.get(url, headers=headers)
    x = response.text
    jsonx = json.loads(x)
    photo = jsonx["photos"][0]["src"]["original"]
    return photo



def get_weather_data(city, state):
    url =f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},"US"&limit=1&appid={Okey}'
    response = requests.get(url)
    x = response.text
    jsonx = json.loads(x)
    test = jsonx[0]
    lat = test["lat"]
    long = test["lon"]
    url =f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={long}&appid={Okey}'
    response = requests.get(url)
    x = response.text
    jsonx = json.loads(x)
    far = (int(jsonx["main"]["temp"]) - 273) * 9/5 + 32
    return jsonx["weather"][0]["description"], far

    
